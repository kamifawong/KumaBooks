//
//  RecentViewController.swift
//  KumaBooks
//
//  Created by Kuma Fai on 21/11/2017.
//  Copyright © 2017 KumaKuma. All rights reserved.
//

import UIKit

class RecentViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            NetworkManager.connectServer(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
