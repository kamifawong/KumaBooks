//
//  NetworkManager.swift
//  KumaBooks
//
//  Created by Kuma Fai on 21/11/2017.
//  Copyright © 2017 KumaKuma. All rights reserved.
//

import UIKit

class NetworkManager: NSObject, NSURLConnectionDelegate {
    
    static var connectName: String? = nil
    static var connectPass: String? = nil
    static var connectValid: Bool! = false
    static var connectRequest: URLRequest!
    static var dataRequest: URLRequest!
    static let alertController = UIAlertController(title: "Connect", message: nil, preferredStyle: .alert)
    
    
    class func connectServer(_ delegate: UIViewController) {
        
        if connectValid == false {
            
            alertController.addTextField { (nameField) in
                nameField.placeholder = "Enter Name"
            }
            alertController.addTextField { (passField) in
                passField.placeholder = "Enter Pass"
            }
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            let connectAction = UIAlertAction(title: "Connect", style: .default) { (action) in
                
//                let name = alertController.textFields![0].text!
//                let pass = alertController.textFields![1].text!
                let name = "public"
                let pass = "karisama"
                let base64LoginString = String(format: "%@:%@", name, pass).data(using: String.Encoding.utf8)!.base64EncodedString()
                
                connectRequest = URLRequest(url: URL(string: "https://novel.kari.moe/dmzj/")!)
                connectRequest.httpMethod = "GET"
                connectRequest.addValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")

                let connectTask = URLSession.shared.dataTask(with: connectRequest) { (data, response, error) in
                    if error != nil {
                        print("ERROR.")
                        return
                    }
                    if let status = response as? HTTPURLResponse {
                        if status.statusCode == 200 {
                            connectValid = true
                            connectName = name
                            connectPass = pass
                            do {
                                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                                print(json)
                            } catch { }
                        } else {
                            print("Failed")
                        }
                    } else {
                        print("CANT LET")
                    }
                }
                connectTask.resume()
            }
            alertController.addAction(connectAction)
            
            delegate.present(NetworkManager.alertController, animated: true, completion: nil)
            
        }
        
        dataRequest = URLRequest(url: URL(string: "https://novel.kari.moe/")!)
        dataRequest.httpMethod = "GET"
        let dataTask = URLSession.shared.dataTask(with: dataRequest) { (data, response, error) in
                do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        print(json)
                    } catch { }
        }
        dataTask.resume()
    }
    
    class func retrieveData() {
        
    }
    
    
}
